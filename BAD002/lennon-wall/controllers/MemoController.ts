import {Request, Response} from 'express';
import {MemoService} from '../services/MemoService';
import socketIO from 'socket.io';


export class MemoController {
    constructor(private memoService: MemoService, private io: SocketIO.Server) {}

    getMemos = async(req: Request,res: Response) => {
        try{
            const result = await this.memoService.getMemos();
            res.json(result);
        } catch(e) {
            console.error(e.message);
            res.status(500).json({message: 'internal server error'})
        }
    }

    addMemo = async()(req: Request,res: Response) => {
        try{
            const content = req.body.content
            const memoID = this.memoService.addMemo(content, req.file.filename);
            this.io.emit('new-memo',{
                content,
                image:req.file.filename,
                id: memoID
            });
            res.json({success:true});
        } catch(e) {
            console.error(e.message);
            res.status(500).json({message: 'internal server error'})
        }
    }
};