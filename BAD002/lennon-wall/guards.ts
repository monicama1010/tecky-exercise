import {Request,Response,NextFunction} from 'express';

export const isLoggedIn = function(req:Request,res:Response,next:NextFunction){
    console.log(req.session);
    if(req.session){
        if(req.session.user){
            next();
            return;
        }
    }

    res.redirect('/');
}
