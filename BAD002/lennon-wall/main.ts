import express from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
import path from 'path';
import multer from 'multer';
import pg from 'pg';
import dotenv from 'dotenv';
import http from 'http';
import socketIO from 'socket.io';
// import grant from 'grant-express';




dotenv.config(); // 行咗呢句，先有下面果啲process.env

// Setup 駁去 DB 嘅connection(電話線)
export const client_WSP011 = new pg.Client({
    user:process.env.DB_USERNAME_WSP011,
    password:process.env.DB_PASSWORD_WSP011,
    database:process.env.DB_NAME_WSP011,
    host:"localhost", 
    port:5432 // always this one
});
// 真係撥通個電話
client_WSP011.connect();


// Server side code    X accessible by end-users
const app = express();
const server = new http.Server(app);
export const io = socketIO(server);



app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })
export const upload = multer({storage})


// app.use(grant({
//     "defaults":{
//         "protocol": "http",
//         "host": "localhost:8080",
//         "transport": "session",
//         "state": true,
//     },
//     "google":{
//         "key": process.env.GOOGLE_CLIENT_ID || "",
//         "secret": process.env.GOOGLE_CLIENT_SECRET || "",
//         "scope": ["profile","email"],
//         "callback": "/login/google"
//       },
// }));

// memoRoutes 必須係io , client, upload  initialized之後，否則呢幾個value 係undefined

import {MemoService} from './services/MemoService';
import {MemoController} from './controllers/MemoController';

const memoService = new MemoService(client_WSP011);
export const MemoController = new MemoController(memoService);

import {routes} from './routes';
app.use('/test', routes);

import { memoRoutes } from './memoRoutes';
import { userRoutes } from './userRoutes';
import { isLoggedIn } from './guards';
import { logger } from './logger';
app.use('/',memoRoutes);
app.use('/',userRoutes);


app.use(function(req,res,next){
    logger.info(req.path);
    next();
});

app.use(express.static('public'));
app.use(isLoggedIn,express.static('protected'));

app.use(function(req,res){
    res.sendFile(path.join(__dirname,'public/404.html'));
})


server.listen(8080,function(){
    console.log("Express listening at http://localhost:8080");
})


io.on('connection', function (socket) {
    console.log(socket);
    if(!socket.request.session?.user){
        socket.disconnect()
    }
});