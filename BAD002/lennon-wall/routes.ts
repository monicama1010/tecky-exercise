import express from 'express';
import { upload } from './main';
import {memoController} from './main';

export const routes = express.Router();
routes.get('/memos/, memoController.getMemos');
routes.post('/memos',upload.single('image'),memoController.addMemo);