import { Client } from 'pg';

export class MemoService {
    constructor(private client_WSP011: Client) { };

    async getMemos() {
        const result = await this.client_WSP011.query(`SELECT * FROM memos order by updated_at desc`);
        return result.rows;
    }


async addMemo(content: string, filename: string) {
   const memoID = await this.client_WSP011.query(/*sql*/`INSERT INTO memos (content,image,created_at,updated_at) 
   values ($1,$2,NOW(),NOW()) RETURNING id`,
        [content, filename]);
        return memoID.rows[0].id;
}
}