import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { Timer } from './Timer';
import { TimerFunc } from './TimerFunc';

function App() {
  const [second, setSecond] = useState(0)
  const [isOpen, setIsOpen] = useState(false)

  // mount 的時，行一獲
  useEffect(() => {
    setInterval(() => {
      setSecond(second => second + 1)
    }, 1000);
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          00:00:{second}
        </p>
        <button onClick={() => setIsOpen(!isOpen)}>+</button>
        { isOpen && <Timer /> }
        { isOpen && <TimerFunc /> }
      </header>
      <div class=" d-table w-100">
  <div class="d-table-cell tar w-10">
 <audio id="player2" src="http://www.mp3naat.com/download/owais-raza-qadri/mustafa-jaan-e-rehmat.mp3"></audio>    
<a onclick="document.getElementById('player2').play()"><i  class="fa fa-play-circle-o fa-2x"></i></a>
<a onclick="document.getElementById('player2').pause()"><i  class="fa fa-pause-circle-o fa-2x"></i></a> 
  </div>
</div>
    </div>
    
  );
}

export default App;

