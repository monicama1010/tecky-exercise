import React from 'react';

export class Timer extends React.Component<{}, {second: number}> {
  private timer: number = 0;

  public constructor(props: {}) {
    super(props);

    this.state = {
      second: 0
    };
  }

  public componentDidMount() {
    this.timer = window.setInterval(() => {
      console.log('加一！')
      this.setState(state => ({
        second: state.second + 1
      }))
    }, 1000);
  }

  public componentWillUnmount() {
    window.clearInterval(this.timer)
  }

  public render() {
    return (
      <p>
        00:00:{this.state.second}
      </p>
    )
  }
  
}
