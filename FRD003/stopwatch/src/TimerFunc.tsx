import React, { useState, useEffect } from 'react';

export function TimerFunc() {
  const [second, setSecond] = useState(0);

  // mount 時做的事
  useEffect(() => {
    const timer = setInterval(() => {
      console.log('加一 fun！')
      setSecond(second => second + 1)
    }, 1000)

    // unmount 時做的事
    return () => {
      clearInterval(timer)
    }
  }, [])

  return (
    <p>
      00:00:{second}
    </p>
  )

}
