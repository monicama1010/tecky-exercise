function checkMarkSix (result, bid){
    let count = 0;
    for (let num of result){
    for (let bidNum of bid){
        if(num === bidNum){
            count ++;
        }
    }
}
return count === 2;
}


function quickPicks(result, numberOfBids){
    const bids = [];
    while (bids.length < numberOfBids){
        const bid = [];
        while (bid.length <2){
            const randomNum = Math.floor(Math.random()*20);
            if (!bid.includes (randomNum)){
                bid.push (randomNum);
            }
        }
        bids.push(bid);
    }
    const bidResults = []
    for (let bid of bids){
        const bidResult = {
        bid: bid,
        win:checkMarkSix(result, bid)
    }
    bidResults.push(bidResult);
}
    return bidResults;
}

console.log(quickPicks([1, 3, 5, 7, 9, 11], 1)); 
console.log(quickPicks([1, 3, 5, 7, 9, 11], 3));
