function rnaTranscription(dna){
    let rna = "";
    for (let nucleotide of dna){
        if (nucleotide === "G"){
            rna += "C"
        }else if (nucleotide === "C"){
            rna += "G"
        }else if (nucleotide === "T"){
            rna += "A"
        }else if (nucleotide === "A"){
            rna += "U"
        }
    }
    return rna;
}
console.log(rnaTranscription("GCTAGCT"));