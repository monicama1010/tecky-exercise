const objectArr = [{"name":"Hong Kong","topLevelDomain":[".hk"],"alpha2Code":"HK","alpha3Code":"HKG","callingCodes":["852"],"capital":"City of Victoria","altSpellings":["HK","香港"],"region":"Asia","subregion":"Eastern Asia","population":7324300,"latlng":[22.25,114.16666666],"demonym":"Chinese","area":1104.0,"gini":53.3,"timezones":["UTC+08:00"],"borders":["CHN"],"nativeName":"香港","numericCode":"344","currencies":[{"code":"HKD","name":"Hong Kong dollar","symbol":"$"}],"languages":[{"iso639_1":"en","iso639_2":"eng","name":"English","nativeName":"English"},{"iso639_1":"zh","iso639_2":"zho","name":"Chinese","nativeName":"中文 (Zhōngwén)"}],"translations":{"de":"Hong Kong","es":"Hong Kong","fr":"Hong Kong","ja":"香港","it":"Hong Kong","br":"Hong Kong","pt":"Hong Kong","nl":"Hongkong","hr":"Hong Kong","fa":"هنگ‌کنگ"},"flag":"https://restcountries.eu/data/hkg.svg","regionalBlocs":[],"cioc":"HKG"}]
const hongKong = objectArr [0];
for (let key in hongKong){
    if (key === "currencies"){
        for (let currency of hongKong.currencies){
            for (let currencyKey in currency){
                console.log(`Currencies_${currencyKey}:${currency[currencyKey]}`)
            }
        }

    }else if(key ==="languages"){
        for(let language of hongKong.languages){
            for(let languageKey in language){
                console.log(`languages_${languageKey}:${language[languageKey]}`)
            }
        }
    }else if(key ==="translations"){
        for(let translationKey in hongKong.translations){
            console.log(`Translations_${translationKey}:${hongKong.translations[translationKey]}`)

        }
    }else{
        console.log(`${key[0].toUpperCase()+key.slice(1)}:${hongKong[key]}`);
    }
}