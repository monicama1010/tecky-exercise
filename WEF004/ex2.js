const objectArr = [{"name":"Hong Kong","topLevelDomain":[".hk"],"alpha2Code":"HK","alpha3Code":"HKG","callingCodes":["852"],"capital":"City of Victoria","altSpellings":["HK","香港"],"region":"Asia","subregion":"Eastern Asia","population":7324300,"latlng":[22.25,114.16666666],"demonym":"Chinese","area":1104.0,"gini":53.3,"timezones":["UTC+08:00"],"borders":["CHN"],"nativeName":"香港","numericCode":"344","currencies":[{"code":"HKD","name":"Hong Kong dollar","symbol":"$"}],"languages":[{"iso639_1":"en","iso639_2":"eng","name":"English","nativeName":"English"},{"iso639_1":"zh","iso639_2":"zho","name":"Chinese","nativeName":"中文 (Zhōngwén)"}],"translations":{"de":"Hong Kong","es":"Hong Kong","fr":"Hong Kong","ja":"香港","it":"Hong Kong","br":"Hong Kong","pt":"Hong Kong","nl":"Hongkong","hr":"Hong Kong","fa":"هنگ‌کنگ"},"flag":"https://restcountries.eu/data/hkg.svg","regionalBlocs":[],"cioc":"HKG"}]
const hongKong = objectArr [0];
const hongKongKeys = Object.keys(hongKong);

const resultString = hongKongKeys.map(function(key){
    if (key === "currencies"){
        return hongKong.currencies.map(function(currency){const currencyKeys = Objects.keys(currency)
        return currencyKeys.map(function(currencyKey){
            return `Currencies_${currencyKey}:$currency[currencyKey]}`;
        }).join("\n");
    }).join("\n")


        }else if (key === "languages"){
            return hongKong.languages.map(function(language){
                const languageKeys = Object.keys(language),

                return languageKeys.map(function(languageKey){
                    return `languages_${languageKey}:{language[languageKey]}`;
                                }).join("\n")
            }else if(key ==="translations"){
                const translationKeys = Objects.keys(hongKong.translations);
                return translationKeys.map(function(translationKey){
                    return`Translations_${translationKey}:$(hongKong.translations[translationsKey]}`
                }).join("\n"),

            }else {
                return`$(key[0].toUpperCase()+key.slice(1)}:${hongKong[key]}`;
        }
    })
console.log(resultString.join("\n"));