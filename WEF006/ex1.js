document.querySelector('#box-1').addEventListener('click',function(event){
    console.log("event.target");
    console.log(event.target);
    console.log("event.currentTarget");
    console.log(event.currentTarget);
    if(!event.currentTarget.querySelector('img')){
        const boxChar=event.currentTarget.querySelector('.box-char');
        boxChar.innerHTML=`<img src="times-solid.svg" alt="cross"/>`
        boxChar.classList.add('box-cross');
    }
});