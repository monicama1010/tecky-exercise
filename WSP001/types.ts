
//findFactors

function findFactors(num:number){
    let factors = [];
    for(let factor = 2; factor <= num / 2 ; factor++){
        if(num % factor === 0){
           factors.push(factor);
        }
    }
    return factors;
 }

 console.log(findFactors(200));


 //LeapYear

let year = 2000;

if (year % 400 === 0) {
    console.log("Leap Year");
} else if (year % 100 === 0) {
    console.log("Not a Leap Year");
} else if (year % 4 === 0) {
    console.log("Leap Year");
} else {
    console.log("Not a Leap Year");
}

//rnaTranscription

 function rnaTranscriptionIfElse(dna:string){
    let rna = "";
    for(let nucleotide of dna){
        if(nucleotide === "G"){
            rna += "C"
        }else if (nucleotide === "C"){
            rna += "G"
        }else if (nucleotide === "T"){
            rna += "A"
        }else if (nucleotide === "A"){
            rna += "U"
        }else{
            throw new Error(`The nucleotide ${nucleotide} does not exist`)
        }
    }
    return rna;
}
let dna = "GCTAGCT";
console.log(rnaTranscriptionObj("GCTAGCT"));


//factorial

 function factorial(number:number):number{
    if(number === 0 || number === 1){
       return 1;
    }
 
    return factorial(number - 1) * number
 }
console.log(factorial(10));
//the type of the following object

 const peter = const peter: {
    name: string;
    age: number;
    students: ({
        name: string;
        age: number;
        exercises?: undefined;
    } | {
        name: string;
        age: number;
        exercises: {
            score: number;
            date: Date;
        }[];
    })[];
}


//setTimeout
const timeoutHandler = ()=>{
    console.log("Timeout happens!");
}

const timeout = 1000;

setTimeout(timeoutHandler,timeout);

// the type of the variable of someValue
const someValue = Math.random() > 0.5? 12: null;