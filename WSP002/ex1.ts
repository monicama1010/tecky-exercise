// Declaration of Class and its methods
class Player{

    private name:string;
    private strength:number;
    
    constructor(strength:number,name:string){
        this.strength  = strength;
        this.name = name;
    }


    attack(monster:Monster){
        const isCritical = Math.random()>0.8;
        const multiplier = isCritical? 2:1;
        monster.injure(this.strength * multiplier);
        console.log(`Player ${this.name} attacks a monster (HP: ${monster.getHp()}) ${isCritical? "[CRITICAL]":""}`);

        if(monster.getHp() === 0){
            this.gainExperience(5);
        }
    }



    gainExperience(exp:number){
this.strength += exp;
console.log(`Exp Gain! ${this.name} gains ${exp} in strength`)
    }

}


class Monster{
    // Think of how to write injure

    private hp:Number;
constructor(){
    this.hp=300;
}
    injure(damage:number){
this.hp = Math.max(this.hp - damage,0);
    }

    getHp() {
        return this.hp;
    }
}



// Invocations of the class and its methods
const peter = new Player(20,'Peter');
const monster = new Monster();
const john = new Player(30,'John');
// English counterpart: Player attacks monster

while (monster.getHp() > 0){
    peter.attack(monster);
    if(monster.getHp() === 0){
        break;
    }
    john.attack(monster);
}