interface Attack{
    damage:number
 }
 
 class BowAndArrow implements Attack{
      constructor(public damage:number){};
 }
 
 class ThrowingSpear implements Attack{
    constructor(public damage:number){};
 }

 class Swords implements Attack{
 constructor(public damage:number){};
 }

 class MagicSpells implements Attack{
 constructor(public damage:number){};
 }
 
 interface Player{
     attack(monster:Monster): void;
     switchAttack(): void;
     gainExperience(exp:number): void;
 }
 
 class Amazon implements Player{
     private primary: Attack;
     private secondary: Attack;
     private usePrimaryAttack: boolean;
     constructor(){
         this.primary = new BowAndArrow(30);
         this.secondary = new ThrowingSpear(40);
         this.usePrimaryAttack = true;
     }
         gainExperience(exp: number):void{
             console.log(`Gain ${exp} experience!`);
      }
 
      attack(monster:Monster):void{
          const isCritical = Math.random() > 0.8;
          const multiplier = isCritical? 2:1;
          if(usePrimaryAttack){
            monster.injure(this.primary.damage*multiplier);
            console.log(`Amazon attacks a monster (HP: ${monster.getHp()})with Bow and Arrow ${isCritical? "[CRITICAL]":""}`);
          }else{
            monster.injure(this.secondary.damage*multiplier);
            console.log(`Amazon attacks a monster (HP: ${monster.getHp()})with Throwing Spear ${isCritical? "[CRITICAL]":""}`);
          }
          if(monster.getHp() === 0){
              this.gainExperience(5);
          }
      }
 
      switchAttack(){
          this.usePrimaryAttack = !this.usePrimaryAttack;
      }
 }
 
 export class Monster{
     private hp:number;
     constructor(){
         this.hp = 700;
     }

     injure(damage:number){
         this.hp = Math.max(this.hp - damage,0);
     }

     getHp(){
         return this.hp;
     }
 }

 const amazon = new Amazon();
 const paladin = new Paladin();
 const barbarian = new Barbarian();
 const monster = new Monster();

 while (monster.getHp()>0){
     amazon.attack(monster);
     amazon.switchAttack();
     paladin.attack(monster);
     paladin.switchAttack();
     barbarian.attack(monster);
     barbarian.switchAttack();

 }