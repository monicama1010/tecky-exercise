import fs from 'fs';
import path from 'path';


function listAllJsRecursive(folderPath: string){
    fs.readdir(folderPath,function(err,files){
        if(err){
            console.log("Failed! Sosad. "+err.message);
        }else{
            for(let file of files){
                fs.stat(path.join(folderPath,file),function(err,stats){
                    if(err){
                        console.log("Failed! Sosad. "+err.message);
                    }else{
                        if(stats.isFile() && file.endsWith('.js')){
                            console.log(path.join(path.resolve(folderPath),file));
                        }else if(stats.isDirectory()){
                            listAllJsRecursive(path.join(folderPath,file));
                        }else{
                        }
                    }
                })
            }
        }
    })
}

listAllJsRecursive('./');


