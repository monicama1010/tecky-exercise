
import fs from 'fs';
import path from 'path';


export async function listAllJsRecursive(folderPath: string){
    try{
        let files = await fsReadDirPromise(folderPath); // files is now array of filenames.
        for(let file of files){
            const stats = await fsStatPromise(path.join(folderPath,file));
            if(stats.isFile() && file.endsWith('.js')){
                console.log(path.join(path.resolve(folderPath),file));
                // Base case 1
            }else if(stats.isDirectory()){
                listAllJsRecursive(path.join(folderPath,file));// Recursive statement
            }else{
                // Base case 2
                // Do nothing
            }
        }
    }catch(err){
        console.log("Failed! Sosad. "+err.message);
    }
}
function fsReadDirPromise(folderPath:string){
    // Promise 係沙嗲牛麵嘅單
    return new Promise<string[]>(function(resolve,reject){
        fs.readdir(folderPath,function(err,files){
            // 當你讀完個folder(Async operation 之後)發生嘅嘢
            if(err){
                // err係間餐廳同你講無牛肉
                reject(err);
            }else{
                // files 係個沙嗲牛麵
                resolve(files);
            }
        });
    });
}

// 佢幫你手做晒所有promisify嘅工作
// const fsReadDirPromise = util.promisify(fs.readdir); //呢句做嘅嘢，同你做嘅一樣
fsReadDirPromise('./')
    .then((data)=>console.log(data));

function fsStatPromise(filePath:string){
    // Promise 係沙嗲牛麵嘅單
    return new Promise<fs.Stats>(function(resolve,reject){
        fs.stat(filePath,function(err,stats){
            // 當你讀完個folder(Async operation 之後)發生嘅嘢
            if(err){
                // err係間餐廳同你講無牛肉
                reject(err);
            }else{
                // stats 係個沙嗲牛麵
                resolve(stats);
            }
        });
    });
}
// const fsStatPromise = util.promisify(fs.stat); //呢句做嘅嘢，同你做嘅一樣
fsStatPromise('./index.js').then(console.log);

