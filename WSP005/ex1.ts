import { readlinePromise, listAllJsRecursive } from './lib'; // from previous exercise
import os from 'os';
import fs from 'fs';

const readCommand = async ()=>{
    while(true){ // game-loop, eval-loop
        // Exit by Ctrl+C
        const answer = await readlinePromise("Please choose read the report(1) or run the benchmark(2):");
        const option = parseInt(answer,10);
        console.log(`Option ${answer} chosen.`);
        if(option == 1){
            await readTheReport();  
        }else if(option == 2){
            await runTheBenchmark(); 
        }else{
            console.log("Please input 1 or 2 only.");
        }
    }
}

readCommand();

async function runTheBenchmark(){
    const beforeOnceFreeMem = os.freemem();
    const beforeOnce = new Date(); // 前
    await listAllJsRecursive('./');
    const afterOnce = new Date(); //後
    const afterOnceFreeMem = os.freemem();

    console.log(afterOnce.getTime() - beforeOnce.getTime());
    console.log(afterOnceFreeMem - beforeOnceFreeMem);
    const beforeHundredTimes = new Date();
    for(let i = 0; i<100 ;i++){
        await listAllJsRecursive('./');
    }
    const afterHundredTimes = new Date();

    console.log(afterHundredTimes.getTime() - beforeHundredTimes.getTime());

    const results:Report = [{
        startDate: beforeOnce.toISOString(),
        endDate:afterOnce.toISOString()
        freeMemory
    }];

    fs.promises.writeFile('result.json',JSON.stringify(results));
}

type Report = Trial[] 

interface Trial{
    freeMemory: number
    startDate: string
    endDate:string
}

async function readTheReport(){
    fs.promises.readFile('result.json',JSON.parse(results)); 
}
