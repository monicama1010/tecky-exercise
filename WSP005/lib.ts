import readline from 'readline';
import path from 'path';
import fs from 'fs';

export function readlinePromise(question:string){
    return new Promise<string>((resolve,reject)=>{
        const readLineInterface = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        readLineInterface.question(question,(answer:string)=>{
            resolve(answer); // resolve ，將成功讀到嘅data/ result 畀返出便
            readLineInterface.close();
        });
    })
}


// Async/Await 版本嘅listAllJsRecursive
export async function listAllJsRecursive(folderPath: string){
    try{
        let files = await fsReadDirPromise(folderPath); // files is now array of filenames.
        for(let file of files){
            const stats = await fsStatPromise(path.join(folderPath,file));
            if(stats.isFile() && file.endsWith('.js')){
                // console.log(path.join(path.resolve(folderPath),file));
                // Base case 1
            }else if(stats.isDirectory()){
                await listAllJsRecursive(path.join(folderPath,file));// Recursive statement
            }else{
                // Base case 2
                // Do nothing
            }
        }
    }catch(err){
        console.log("Failed! Sosad. "+err.message);
    }
}

function fsReadDirPromise(folderPath:string){
    return new Promise<string[]>(function(resolve,reject){
        fs.readdir(folderPath,function(err,files){
            if(err){
                reject(err);
            }else{
                resolve(files);
            }
        });
    });
}

function fsStatPromise(filePath:string){
    return new Promise<fs.Stats>(function(resolve,reject){
        fs.stat(filePath,function(err,stats){
            if(err){
                reject(err);
            }else{
                resolve(stats);
            }
        });
    });
}
