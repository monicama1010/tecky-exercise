import express, { NextFunction } from 'express'; // import express framework
import expressSession from 'express-session';
import {Request,Response} from 'express';
import path from 'path';

// Create 一個新express application , 一個express app ，就霸住一個port
const app = express();

// 當有request 去到 path 係／，就做呢個function 嘅code
// '/' 代表個Route/Path ，無寫就對應 /
// app.get('/',function(req:Request,res:Response){ // handler/listener
//     res.end("Hello World!");
// });

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}))


app.get('/abcdef',function(req:Request,res:Response){ // handler/listener
    res.end("You try to get /abcdef!");
})

app.get('/website.html',function(req:Request,res:Response,next:NextFunction){

    // res.sendFile(path.join(__dirname,'public/index.html'));

    console.log("I am in the 截胡function!!");
    if(req.session){

        console.log(`My req.session counter is ${req.session.counter}`);
        req.session.counter = !req.session.counter ? 1: req.session.counter+ 1;
    }

    next();
})

// 逢你有乜嘢URL，你就入去睇下Public 呢個folder 睇下有無對應的file 名
app.use(express.static('public'));

app.use(function(req,res){
    res.sendFile(path.join(__dirname,'public/404.html'));
})

//  現家隻app 係8080 port listen
// 去到呢度先開始霸住個port
app.listen(8080,function(){
    console.log("Listening at http://localhost:8080")
});

