
//Create a route /memos which read all of the content 
//of memos.json and responds with res.json()
//Create a login form that submit with method POST. 
//Create a route called /login that reads the request data from req.body. 
//Check against the username and password stored in the users.json.
//If the username and password is correct, update req.session.user to store the user data to represent a verified session. 
//Create an extra page called admin.html which is mean to be accessed only when the user has logged in. 
//If the username and password is incorrect, redirect to the index page.
//Create a guard middleware called isLoggedIn, 
//it should verify if the req.session.user is true . In case it is true, 
//then you can call the next function, otherwise the request should be redirected to the index page.
//You can protect a folder called protected with the following syntax.

import express, { Request, Response, NextFunction } from 'express';
import expressSession from 'express-session';
import path from 'path';
import bodyParser from 'body-parser';
//import fs from 'fs';
//import multer from 'multer';


// Server side code    X accessible by end-users
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
//fs.promises.writeFile('memos.json',JSON.stringify("user-input",null,4));
app.post('/memos',upload.single('images'),async function(req,res,next){
    console.log(req.file.filename);
    const {user-input,images} = req.body;
    console.log(req.body);


app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true

}));


app.get('/', function (req: Request, res: Response, next: NextFunction) {

    if (req.session) {
        if (req.session.counter) {
            req.session.counter += 1;
        } else {
            req.session.counter = 1;
        }
        console.log(`Counter is ${req.session.counter}`);
    }
    next();
})


app.use(function (req, res, next) {
    console.log(`[${new Date().toISOString()}] Request ${req.path}`);
    next();
});



app.use(express.static('public'));

app.use(function (req, res) {
    res.sendFile(path.join(__dirname, 'public/404.html'));
})


app.listen(8080, function () {
    console.log("Express listening at http://localhost:8080");
})
