CREATE TABLE memos(
     id SERIAL,
     content TEXT,
     image TEXT,
     created_at TIMESTAMP,
     updated_at TIMESTAMP
);

CREATE TABLE users(
    id SERIAL,
    username varchar(255),
    password varchar(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);
