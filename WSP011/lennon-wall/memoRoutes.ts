import express,{Request,Response} from 'express';
import { upload, client_WSP011, io } from './main';
import { logger } from './logger';

export const memoRoutes = express.Router();
// / + /memos  => /memos
// /memos + / => /memos
memoRoutes.post('/memos',upload.single('image'),addMemo);
memoRoutes.put('/memos/:id',updateMemo);
memoRoutes.delete('/memos/:id',deleteMemo);
memoRoutes.get('/memos',getMemos);





async function getMemos(req:Request,res:Response){
    const result = await client_WSP011.query(`SELECT * FROM memos order by updated_at desc`);
    res.json(result.rows);
};

async function addMemo(req:Request,res:Response){
    const content = req.body.content;
    const result = await client_WSP011.query(/*sql*/`INSERT INTO memos (content,image,created_at,updated_at) values ($1,$2,NOW(),NOW()) RETURNING id`,
            [content,req.file.filename]);
    console.log(result.rows);
    io.emit('new-memo',{
        content,
        image:req.file.filename,
        id: result.rows[0].id
    });
    res.json({success:true});
}

async function updateMemo(req:Request,res:Response){
    try{
        const memoId = parseInt(req.params.id);
        (memoId as any).abc();
        if(isNaN(memoId)){
            res.status(400).json({msg:"id is not a number!"});
            return;
        }
        const {content} = req.body;
        await client_WSP011.query(`UPDATE memos set content = $1,updated_at = NOW() WHERE id = $2`,[content,memoId]);
        res.json({success:true});
    }catch(e){
        logger.info(e);
        res.status(500).json({msg:"[MEMO001]Failed to update memo"});
    }
}

async function deleteMemo(req:Request,res:Response){
    const memoId = parseInt(req.params.id);
    if(isNaN(memoId)){
        res.status(400).json({msg:"id is not a number!"});
        return;
    }
    await client_WSP011.query(`DELETE FROM memos where id = $1` ,[memoId]);
    res.json({success:true});
}