import express,{Request,Response} from 'express';
import { client_WSP011 } from './main';
import { User } from './models';
import { checkPassword, hashPassword } from './hash';
import { isLoggedIn } from './guards';
// import fetch from 'node-fetch';

export const userRoutes = express.Router();
userRoutes.post('/login',login);
// userRoutes.get('/login/google',loginGoogle);
userRoutes.get('/logout',logout);
userRoutes.post('/users',isLoggedIn,createUser);


async function login(req:Request,res:Response){
    const {username,password} = req.body;
    const result = await client_WSP011.query(`SELECT * from users where username = $1`,[username]);
    const users:User[] = result.rows;
    const user = users[0];
    if(user && await checkPassword(password,user.password)){
        // login 成功
        if(req.session){
            req.session.user = user; // 係個session 度mark 咗你係成功login 咗
        }
        res.redirect('/admin.html');
    }else{
        // login 失敗
        res.status(401).redirect("/?error=login+failed");
    }
};


// async function loginGoogle(req:Request,res:Response){
//     const accessToken = req.session?.grant.response.access_token;
//     const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
//         method:"get",
//         headers:{
//             "Authorization":`Bearer ${accessToken}`
//         }
//     });
//     const result = await fetchRes.json();
//     const users = (await client.query(`SELECT * FROM users where username = $1`,[result.email])).rows;
//     let user = users[0];
//     if(!user){
//         // 開返個新account
//         user = (await client.query(`INSERT INTO users (username,password) values ($1,$2) RETURNING *`
//             ,[result.email,await hashPassword(Math.random().toString(36).substring(2))])).rows[0];
//     }

//     if(req.session){
//         req.session.user =  user;
//     }
//     res.redirect('/admin.html');
// }

async function createUser(req:Request,res:Response){
    const {username,password} = req.body;
    await client_WSP011.query(`INSERT INTO users (username,password) values ($1,$2)`
            ,[username,await hashPassword(password)]);
    res.json({success:true});
}

async function logout(req:Request,res:Response){
    if(req.session){
        delete req.session.user;
    }
    res.redirect('/')
}