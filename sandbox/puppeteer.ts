import jsonfile from 'jsonfile';
import puppeteer from 'puppeteer';
import { location,} from './bf-pre-scraping';
import { JobData } from './bf-pre-scraping'

async function scraper() {
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 250, // slow down by ms
        executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'
    });
    let page = await browser.newPage();
 const JobData:JobData [] = [];

    const locationLen = location.length;

        try {
            for (let locationIndex = 0; locationIndex < locationLen; locationIndex++) {
                console.log(`Now is ${location[locationIndex]}`);
                    let jdbUrl = `https://hk.jobsdb.com/hk/information-technology-software-development-jobs-${location[locationIndex]}1`//?SalaryF=${salaryArr[a][0]}&SalaryT=${salaryArr[a][1]}&SalaryType=1`
                    await page.goto(jdbUrl, { waitUntil: 'networkidle2' });

                    console.log("test 1")
                    const dataArray = await page.evaluate(() => {
                        if (document.querySelector('strong')) {
                            let jobUrlArr: string[] = [];
                            let numberOfJob = Number(((document.querySelector('strong')) as any).innerText.slice(2));
                                console.log(`This page has ${numberOfJob} Job Posts`)
                                for (let i = 0; i < numberOfJob; i++) {
                                    let test = document.querySelectorAll('[id^="jobCardHeading_"]>a')[i]
                                    if (test){
                                        let Url = test.getAttribute('href');
                                        if(Url){
                                            console.log(Url)
                                            jobUrlArr.push(Url);
                                        }
                                    }
                                }
                                return jobUrlArr;
                        } 
                        return;    
                    });
                    // go to individaul job page
                    if (dataArray) {
                        let dataArrayLen = dataArray.length
                        if (dataArrayLen > 0) {
                            for (let i = 0; i < dataArrayLen; i++) {
                                let jdbUrl = dataArray[i]
                                console.log(`=====This station is = ${i}=====`);
                                await page.goto(jdbUrl, { waitUntil: 'networkidle2' });
                                //console.log(`opened ${jdbUrl}  `);

                                const job = await page.evaluate(() => {
                                    console.log(`=====page.evaluate running=====`)
                                    try {
                                        let title = (document.querySelector('[class="FYwKg _6Gmbl_1my"]>h1') as any).innerText
                                        let companyName = (document.querySelector('[class="FYwKg _1GAuD C6ZIU_1my _8QVx6_1my _3NFar_1my _2CELK_1my _3srVf_1my _30vru_1my"]') as any).innerText
                                        let location = (document.querySelectorAll('[class="FYwKg _1GAuD C6ZIU_1my _8QVx6_1my _3NFar_1my _29m7__1my"]')[6] as any).innerText
                                        let jd = (document.documentElement as any).innerText
                                        let postDate = (document.querySelectorAll('[class="FYwKg _11hx2_1my"]>span')[1] as any).innerText
                                            
                                        return {
                                            title,
                                            companyName,
                                            location,
                                            jd,
                                            postDate,

                                        }
                                    } catch (err) {
                                        return
                                    }
                                })
                                // check job post date is today or not
                                let todayDate = Date().slice(8, 10)
                                let jobDate = job?.postDate.slice(10, 12)
                                if (todayDate == jobDate) {
                                    if (job) {
                                        job["url"] = jdbUrl
                                        console.log(job);
                                        JobData.push(job);

                                        await jsonfile.writeFile('./jobsDBData/data.json', JobData, { spaces: 4 });
                                    }
                                }else{
                                    i = 30
                                }                                
                            };

                        }
                    }
            };
        } catch (err) {
            console.log(err);
            return
        }
    await browser.close();
    console.log('!!!!!!!!!!!!!!!!!==========update finished===========!!!!!!!!!!!!!!!!!!');
}

scraper()